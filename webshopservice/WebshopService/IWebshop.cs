﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Items;

namespace WebshopService
{
   
    [ServiceContract(Namespace = "WebshopService")]
    public interface IWebshop
    {
        [OperationContract]
        string GetWebshopName();

        [OperationContract]
        List<Item> GetProductList();

        [OperationContract]
        string GetProductInfo(string productId);

        [OperationContract]
        bool BuyProduct(string productId);

        [OperationContract]
        List<Item> x();
    }
}
