﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Items;

namespace WebshopService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Webshop" in both code and config file together.
    public class Webshop : IWebshop
    {
        private List<Items.Item> _items = new List<Items.Item>
        {
            new Item("1", "info 1", 2.0, 5, true),
            new Item("2", "info 2", 35.12, 3, true),
            new Item("3", "info 3", 12, 1, true),
            new Item("4", "info 4", 125.95, 0, false)
        };
        public string GetWebshopName()
        {
            return string.Format("The High Times");
        }

        public List<Item> GetProductList()
        {
            return _items;
        }

        public string GetProductInfo(string productId)
        {
            foreach (var v in _items.Where(v => v.ProductId == productId))
            {
                return v.ProductInfo;
            }
            return "";
        }

        public bool BuyProduct(string productId)
        {
            foreach (var v in _items.Where(v => v.ProductId == productId).Where(v => v.Stock > 0))
            {
                v.Stock--;
                return true;
            }
            return false;
        }

        public List<Item> x()
        {
            return _items;
        } 
    }
}
