﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Items
{
    public class Item
    {
        public string ProductId { get; set; }
        public string ProductInfo { get; set; }
        public double Price { get; set; }
        public int Stock { get; set; }
        public bool OnSale { get; set; }

        public Item(string id, string info, double price, int stock, bool sale)
        {
            this.ProductId = id;
            this.ProductInfo = info;
            this.Price = price;
            this.Stock = stock;
            this.OnSale = sale;
        }
    }
}
