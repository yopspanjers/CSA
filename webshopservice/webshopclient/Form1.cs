﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Items;

namespace webshopclient
{
    public partial class Form1 : Form
    {
        private srWebshop.WebshopClient _proxy;
        private List<Item> _items; 
        public Form1()
        {
            InitializeComponent();
            _proxy = new srWebshop.WebshopClient();
        }

        private void btnGetName_Click(object sender, EventArgs e)
        {
            lblName.Text = _proxy.GetWebshopName();
        }

        private void btnGetInfo_Click(object sender, EventArgs e)
        {
            lblTest.Text = _proxy.GetProductInfo("1");
        }

        private void btnGetList_Click(object sender, EventArgs e)
        { 
            Console.WriteLine("");

        }
    }
}
